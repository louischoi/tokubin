const { JWT } = require('google-auth-library');
const firebaseAdmin = require('firebase-admin');
const axios = require('axios');
const _ = require('lodash');
const {
  KEY_COLLECTION_TOKUBIN21, KEY_DOCUMENT_TIMESTAMP, KEY_DOCUMENT_FARES, KEY_DOCUMENT_FARES_CODE,
  KEY_DOCUMENT_FARES_CABIN, KEY_DOCUMENT_FARES_PRICE,
  KEY_DOCUMENT_FARES_AVAILABLE, KEY_DOCUMENT_FARES_DATE,
} = require('config');
const serviceAccount = require('../firebase.json');

let ready = false;
let apiToken;
let firestoreClient;

module.exports = {
  init: async (logger, firebaseProjectUrl) => {
    firebaseAdmin.initializeApp({
      credential: firebaseAdmin.credential.applicationDefault(),
      databaseURL: firebaseProjectUrl,
    });

    apiToken = await new Promise(((resolve, reject) => {
      const SCOPES = ['https://www.googleapis.com/auth/firebase.remoteconfig'];

      const jwtClient = new JWT(
        serviceAccount.client_email,
        null,
        serviceAccount.private_key,
        SCOPES,
      );
      jwtClient.authorize((err, tokens) => {
        if (err) {
          return reject(err);
        }
        return resolve(tokens.access_token);
      });
    }));

    firestoreClient = firebaseAdmin.firestore();
    ready = true;
  },

  getConfig: async () => {
    if (!ready) {
      throw new Error('firebase not initialized yet');
    }

    const { data } = await axios({
      method: 'get',
      url: 'https://firebaseremoteconfig.googleapis.com/v1/projects/tokubin-f3814/remoteConfig',
      headers: {
        Authorization: ` Bearer ${apiToken}`,
      },
    });
    const { parameters } = data;
    return _.reduce(parameters, (acc, value, key) => ({
      ...acc,
      [key]: JSON.parse(value.defaultValue.value),
    }), {});
  },

  updateAvailabilities: async (logger, from, to, availabilities) => {
    if (!ready) {
      throw new Error('firebase not initialized yet');
    }

    const collectionRef = firestoreClient.collection(KEY_COLLECTION_TOKUBIN21);
    const docId = `${from}-${to}`;
    logger.debug(`update document with id ${docId}`);
    await collectionRef.doc(docId).set({
      [KEY_DOCUMENT_TIMESTAMP]: new Date(),
      [KEY_DOCUMENT_FARES]: _.sortBy(_.map(availabilities, ({
        code, cabin, available, price, date,
      }) => ({
        [KEY_DOCUMENT_FARES_DATE]: date,
        [KEY_DOCUMENT_FARES_CODE]: code,
        [KEY_DOCUMENT_FARES_AVAILABLE]: available,
        [KEY_DOCUMENT_FARES_PRICE]: price,
        [KEY_DOCUMENT_FARES_CABIN]: cabin,
      })), [KEY_DOCUMENT_FARES_DATE, KEY_DOCUMENT_FARES_CODE]),
    }, {
      merge: true,
    });
  },
};
