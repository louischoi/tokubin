const runtimeConfig = require('config');
const _ = require('lodash');
const logger = require('./loggerWrapper');
const FirebaseUtil = require('./FirebaseUtil');
const triggerPipeline = require('./TriggerPipeline');

const [,, ...args] = process.argv;

async function main() {
  logger.debug({ runtimeConfig });

  try {
    const { URL_FIREBASE } = runtimeConfig;
    const [projectId, pipelineToken] = args;
    if (!projectId) {
      throw new Error('bad-request/project-id-invalid');
    }
    if (!pipelineToken) {
      throw new Error('bad-request/pipeline-token-invalid');
    }

    await FirebaseUtil.init(logger, URL_FIREBASE);
    const config = await FirebaseUtil.getConfig(logger);
    logger.debug({ config });

    const subscribedRoutes = _.filter(config.ROUTES_CONFIG || [], { enabled: true });
    await Promise.all(_.map(subscribedRoutes, ({ from, to }) => triggerPipeline(logger, projectId, from, to, pipelineToken))); // eslint-disable-line max-len

    setTimeout(() => {
      process.exit(0);
    }, 500);
  } catch (err) {
    logger.error(err.stack);

    setTimeout(() => {
      process.exit(1);
    }, 500);
  }
}

main();
