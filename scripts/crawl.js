const runtimeConfig = require('config');
const _ = require('lodash');
const logger = require('./loggerWrapper');
const FirebaseUtil = require('./FirebaseUtil');
const checkFlight = require('./CheckFlight');

const [,, ...args] = process.argv;

function distribute(works = [], windowSize = 1) {
  const windows = _.times(windowSize, _.stubArray);
  _.forEach(works, (work, index) => {
    windows[index % windowSize].push(work);
  });
  return windows;
}

async function main() {
  logger.debug({ runtimeConfig });

  try {
    const {
      browserClusterSize, slowMo, headless, skipCSS,
      URL_FIREBASE,
      daysLead, daysCount,
    } = runtimeConfig;
    const [from, to] = args;

    if (!from || !to) {
      throw new Error('empty/invalid flights is provided');
    }

    const dates = _.fill(Array(daysCount), null).map((value, index) => {
      const today = new Date();
      today.setHours(0, 0, 0, 0);
      today.setDate(today.getDate() + index + daysLead);
      return today;
    });

    const browserChunk = _.compact(distribute(dates, browserClusterSize))
      .filter((windows) => !_.isEmpty(windows));
    const results = await Promise.all(browserChunk.map((datesChunk) => checkFlight(logger, {
      slowMo, headless, skipCSS,
    }, datesChunk, from, to)));
    const availabilities = _.flatten(results);

    await FirebaseUtil.init(logger, URL_FIREBASE);
    await FirebaseUtil.updateAvailabilities(logger, from, to, availabilities);

    setTimeout(() => {
      process.exit(0);
    }, 500);
  } catch (err) {
    logger.error(err.stack);

    setTimeout(() => {
      process.exit(1);
    }, 500);
  }
}

main();
