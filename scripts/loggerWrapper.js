const winston = require('winston');

const { combine, timestamp, json } = winston.format;

module.exports = winston.createLogger({
  level: 'debug',
  transports: [
    new winston.transports.Console({
      format: combine(
        timestamp(),
        json(),
      ),
    }),
  ],
});
