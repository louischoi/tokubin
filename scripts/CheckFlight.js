const puppeteer = require('puppeteer');
const _ = require('lodash');

async function taskOpenJEPPage(logger, page) {
  const urlEntry = 'https://www.jal.co.jp/';

  logger.debug(`open page with url ${urlEntry}`);
  await page.goto(urlEntry, {
    timeout: 60000,
  });
}

async function taskSelectOneWayTrip(logger, page) {
  logger.debug('select one way trip');

  await page.click('input#JS_oneWay.form-select-radio');
}

async function taskSelectFromTo(logger, page, from, to) {
  logger.debug(`select from airport: ${from}`);
  logger.debug(`select to airport: ${to}`);

  await page.evaluate((departureAirport, arrivalAirport) => {
    document.querySelector('#JS_departureAirport.form-select-holder').value = departureAirport;
    document.querySelector('#JS_arrivalAirport.form-select-holder').value = arrivalAirport;
  }, from, to);
}

async function taskSelectDate(logger, page, date) {
  const month = date.getMonth() + 1;
  const day = date.getDate();

  logger.debug(`select trip month: ${month}`);
  logger.debug(`select trip day: ${day}`);

  await page.evaluate((departureMonth, departureDay) => {
    document.querySelector('#JS_departureMonth.form-select-holder').value = departureMonth;
    document.querySelector('#JS_departureDay.form-select-holder').value = departureDay;
  }, month, day);
}

async function taskProgressToResult(logger, page) {
  logger.debug('press continue');
  await page.evaluate(() => {
    document.querySelector('a#JS_submitBtn').click();
  });
  await page.waitForSelector('div.list-state-wrapper');
  await page.waitFor(1000);
}

async function taskGetAvailSlots(logger, page) {
  logger.debug('check result rows');
  const segmentElems = await page.$$('tr.js-table-flight-chart_row.JS_classPrice[data-class="y"]');
  logger.debug(`${segmentElems.length} slots found`);

  return Promise.all(_.map(segmentElems, async (segmentElem) => {
    const code = await segmentElem.$eval('.JS_sceduleInfo ._detail-service ._name', (node) => node.innerText);
    const cabin = await segmentElem.$eval('.JS_sceduleInfo ._detail-service ._num .link-txt', (node) => node.innerText);

    const priceButtonElems = await segmentElem.$$('td:not(._cheap) button._cell-item_price');
    const prices = await Promise.all(_.map(priceButtonElems, async (buttonElem) => {
      const available = !buttonElem.disabled;
      let price;
      let label;
      try {
        price = await buttonElem.$eval('._yen', (node) => node.innerText);
        // eslint-disable-next-line no-empty
      } catch (ignored) {
      }
      try {
        label = await buttonElem.$eval('._sup', (node) => node.innerText);
        // eslint-disable-next-line no-empty
      } catch (ignored) {
      }
      return {
        available, price, label,
      };
    }));
    const tokubin = _.find(prices, { label: '特便割引21' });
    return {
      code,
      cabin,
      available: tokubin ? tokubin.available : false,
      price: tokubin ? Number(tokubin.price.replace(/,/g, '')) : null,
    };
  }));
}

module.exports = async (logger, browserOptions = {}, dates = [], airportFrom = '', airportTo = '') => {
  const { slowMo, headless, skipCSS } = browserOptions;

  // initialize
  let availabilities = [];
  let browser;
  let page;
  try {
    browser = await puppeteer.launch({
      headless,
      slowMo,
      args: [
        '--no-sandbox',
        '--disable-setuid-sandbox',
        '--disable-dev-shm-usage',
        '--disable-accelerated-2d-canvas',
        '--disable-gpu',
      ],
    });
    page = await browser.newPage();
    await page.setUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36');
    await page.setRequestInterception(true);
    page.on('request', (request) => {
      const skippedTypes = ['image', 'font', 'stylesheet'];
      if (skipCSS && _.indexOf(skippedTypes, request.resourceType()) !== -1) {
        return request.abort();
      }

      return request.continue();
    });

    for (let i = 0; i < dates.length; i += 1) {
      /* eslint-disable no-await-in-loop */
      await taskOpenJEPPage(logger, page);
      await taskSelectOneWayTrip(logger, page);
      await taskSelectFromTo(logger, page, airportFrom, airportTo);
      await taskSelectDate(logger, page, dates[i]);
      await taskProgressToResult(logger, page);

      // check selected from/to/date
      const selectedFrom = await page.$('select#select-incoming.js-form-condition_incoming[name=boardAirport]');
      const selectedFromOption = await selectedFrom.evaluate((node) => node.value);
      if (selectedFromOption !== airportFrom) {
        throw new Error('departure airport is set incorrectly');
      }
      const selectedTo = await page.$('select#select-outgoing.js-form-condition_outgoing[name=arrivalAirport]');
      const selectedToOption = await selectedTo.evaluate((node) => node.value);
      if (selectedToOption !== airportTo) {
        throw new Error('arrival airport is set incorrectly');
      }
      const selectedMonth = await page.$('input.JS_month[name=boardMonth]');
      const selectedMonthOption = await selectedMonth.evaluate((node) => node.value);
      if (Number(selectedMonthOption) !== dates[i].getMonth() + 1) {
        throw new Error('departure month is set incorrectly');
      }
      const selectedDay = await page.$('input.JS_day[name=boardDay]');
      const selectedDayOption = await selectedDay.evaluate((node) => node.value);
      if (Number(selectedDayOption) !== dates[i].getDate()) {
        throw new Error('departure day is set incorrectly');
      }

      const slots = await taskGetAvailSlots(logger, page);
      availabilities = _.concat(availabilities, _.map(slots, ({
        code, cabin, available, price,
      }) => ({
        code,
        cabin,
        available,
        price,
        from: airportFrom,
        to: airportTo,
        date: dates[i],
      })));
      /* eslint-enable no-await-in-loop */
    }

    return availabilities;
  } catch (e) {
    logger.error(e.message);
    logger.error(e.stack);
    logger.debug({
      url: page.url,
      image: await page.screenshot({
        type: 'jpeg',
        quality: 50,
        fullPage: true,
        encoding: 'base64',
      }),
    });
    throw e;
  } finally {
    if (browser && _.isFunction(browser.close)) {
      await browser.close();
    }
  }
};
