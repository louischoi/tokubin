const axios = require('axios');
const { URL_PIPELINE_TRIGGER } = require('config');

module.exports = async (logger, projectId, from, to, pipelineToken) => {
  const pipelineUrl = URL_PIPELINE_TRIGGER
    .replace(/{{projectId}}/g, projectId)
    .replace(/{{from}}/g, from)
    .replace(/{{to}}/g, to)
    .replace(/{{pipelineToken}}/g, pipelineToken);
  if (logger && logger.debug) {
    logger.debug(`Call ${pipelineUrl}`);
  }

  const { data } = await axios.post(pipelineUrl);
  return data;
};
