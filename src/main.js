import Vue from 'vue';
import {
  Collapse,
  Button,
  Empty,
  Table,
  Tag,
  Divider,
  Spin,
} from 'ant-design-vue';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faDownload,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import './firebase';
import vuex from './vuex';

import App from './App.vue';
import './registerServiceWorker';

Vue.config.productionTip = false;
Vue.use(Button);
Vue.use(Collapse);
Vue.use(Divider);
Vue.use(Empty);
Vue.use(Table);
Vue.use(Tag);
Vue.use(Spin);

library.add(faDownload);
Vue.component('font-awesome-icon', FontAwesomeIcon);

new Vue({
  render: (h) => h(App),
  store: vuex,
}).$mount('#app');
