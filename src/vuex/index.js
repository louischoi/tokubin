import _set from 'lodash/set';
import Vue from 'vue';
import Vuex from 'vuex';
import {
  APP_CONFIG_SET,
} from './mutation-types';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  strict: debug,
  plugins: [],
  state: {
    config: {},
  },
  mutations: {
    /* eslint-disable no-param-reassign */
    [APP_CONFIG_SET]: (state, config) => {
      _set(state, 'config', config);
    },
  },
});
