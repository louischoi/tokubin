const webpack = require('webpack');
const path = require('path');

function resolve(dir) {
  return path.join(__dirname, dir);
}

module.exports = {
  lintOnSave: false,
  pwa: {
    themeColor: '#8B0000',
  },
  pluginOptions: {
    webpackBundleAnalyzer: {
      openAnalyzer: false,
    },
  },
  configureWebpack: {
    plugins: [
      new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en/),
    ],
    resolve: {
      alias: {
        '@ant-design/icons/lib/dist$': resolve('./src/ant-icons.js'),
      },
    },
  },
};
